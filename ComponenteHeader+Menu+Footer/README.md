# Componente Header + Menu + Footer
La idea es crear una pagina web con el contenido basico para que se pueda navegar y que tenga un aspecto atractivo : un header, un menu de navegación y un footer.  La idea es que la pagina cuenta con los elementos de identificacíon (header) y navegación necesarios para que el usuario pueda moverse por nuestro sitio web de forma comoda,además nuestro footer nos permitira ofrecer información adicional por ejmplo localización, aspectos legales ....
Lo ideal es que todas las paginas web de nuestro sitio cuenten con header, footer y menu pues de esta forma nos ayuda a identificar que pese a estar en otra pagina web no hemos salido del sitio web.

Hemos usado la herramienta neocities.org para poder realizar los test de accesibilidad de nuestro componente alcanzando en todos los test exito. (Adjuntamos una captura del resutlado de los test) en el directorio de cada componente. Además tambien se facilita la url de neocities para poder visualizar el componente. Es posible que la visualización desde neocities.org no sea exactamente igual en cuanto a imagenes yy videos con los archivos del .zip debido a que neocities no permite ciertas extensiones de archivos en su version gratuita (.mp4,.jfif,...)

https://componentesa17ivansv.neocities.org/ComponenteHeader+Menu+Footer/index.html


#### Estructura de Directorios
ComponenteHeader+Menu+Footer
    
    CSS 
        componenteheader.css
        componentemenu.css
        componentefooter.css
        
    img
        imagenes-varias
    
    Tests
        Test1Passed.png
        Test2Passed.png

    index.html
    README.md
    