# Proyecto
La idea de este proyecto es intentar realizar una pequeña pagina web utilizando los componentes que fuimos creando en las actividades anteriores para poder unificarlos todos y ver como acutan sobre una misma pagina.
La estuctura no es identica a los componentes generados pues lo suyo es que los componentes creados en primera instancia nos sirvan como base del proyecto pero luego cada uno puede adaptar su contenido en funcion de lo que desee realizar.

El objetivo de este proyecto es conseguir una pagina que integre todo lo visto hasta el momento HTML, JS, CSS y lo que se vio durante este tercer trimestre componentes, contenido multimedia, iconos de fontawesome, accesibilidad, responsive ...

La pagina se compone de la unión de los difrenttes componentes para formar una pagina accesible, iteractiva y dinamica.

IMPORTANTE : Al usar llamadas a funciones "especiales", es necesario abrir la pagina usando un servidor (live server, phpserver ...)


Hemos usado la herramienta neocities.org para poder realizar los test de accesibilidad de nuestro componente alcanzando en todos los test exito. (Adjuntamos una captura del resutlado de los test) en el directorio de cada componente. Además tambien se facilita la url de neocities para poder visualizar el componente. Es posible que la visualización desde neocities.org no sea exactamente igual en cuanto a imagenes yy videos con los archivos del .zip debido a que neocities no permite ciertas extensiones de archivos en su version gratuita (.mp4,.jfif,...)

https://componentesa17ivansv.neocities.org/Proyecto/index.html


#### Estructura de Directorios
Proyecto
    
    CSS 
        componentemasonery.css
    
    img
        imagenes y videos varios
    
    JS
        coleccion.js
        proxectos.json

    Tests
        Test1Passed.png
        Test2Passed.png
    
    VTT
        varios-vtt
    
    coleccion.html
    eventos.html
    galeria.html
    index.html
    registro.html
    README.md
    