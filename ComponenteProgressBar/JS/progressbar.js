//Definimos una variable que contendra una HTML collection con todas las etiquetas con la clase progress2
let lista = document.getElementsByClassName('progress2');
//Recorremos la HTML collection y para cada elemento cogemos el valor del atributo data-progress y lo añadimos a su CSS
for(let i =0;i<lista.length;i++)
{
    let longitud = (lista[i].getAttribute('data-progress'));
    lista[i].style.width = ''+longitud+'%';
}
