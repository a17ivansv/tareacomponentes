# ComponenteProgress-Bar
La idea es crear una pogress-bar con una longitud fija (pillada del atributo data-progress) usando JS y crear una progress-bar animada con CSS.

Hemos usado la herramienta neocities.org para poder realizar los test de accesibilidad de nuestro componente alcanzando en todos los test exito. (Adjuntamos una captura del resutlado de los test) en el directorio de cada componente. Además tambien se facilita la url de neocities para poder visualizar el componente. Es posible que la visualización desde neocities.org no sea exactamente igual en cuanto a imagenes yy videos con los archivos del .zip debido a que neocities no permite ciertas extensiones de archivos en su version gratuita (.mp4,.jfif,...)

https://componentesa17ivansv.neocities.org/ComponenteProgressBar/index.html

#### Estructura de Directorios
ComponenteProgressBar

    CSS 
        componenteprogressbar.css

    JS
        progressbar.js
        
    Tests
        Test1Passed.png
        Test2Passed.png
        
    index.html
    README.md
    