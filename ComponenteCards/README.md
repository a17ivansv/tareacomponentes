# Componente CARDS
La idea de este componente es similar la clase Cards de Bootstrap usando CSS y HTML, además podemos modificarlo minimamente para adaptarlo a nuestra pagina. El objetivo de este componente es tener una base que simule el comportamiento de un Card de Bootstrap pero usando nuestros propios estilos sin depender de ninguna libreria externa. Despues en el proyecto final haremos uso de dicho componente.

Hemos usado la herramienta neocities.org para poder realizar los test de accesibilidad de nuestro componente alcanzando en todos los test exito. (Adjuntamos una captura del resutlado de los test) en el directorio de cada componente. Además tambien se facilita la url de neocities para poder visualizar el componente.

https://componentesa17ivansv.neocities.org/ComponenteCards/index.html


#### Estructura de Directorios
ComponenteCards 
    
    CSS 
        componentecards.css
        
    img      
        lake.jpg 
        river.hpg
    Tests
        Test1Passed.png
        Test2Passed.png
        
    index.html
    README.md
    