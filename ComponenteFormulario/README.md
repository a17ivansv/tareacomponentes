# Componente Formulario
La idea de este componente es crear una plantilla para un formulario iterativo el cual con JS y CSS permitira al usuario enviar los datos y almacenarlos en el navegador mediante localstorage. El objetivo es poder usar dicho componente en el proyecto final realizando las minimas modificaciones

Hemos usado la herramienta neocities.org para poder realizar los test de accesibilidad de nuestro componente alcanzando en todos los test exito. (Adjuntamos una captura del resutlado de los test) en el directorio de cada componente. Además tambien se facilita la url de neocities para poder visualizar el componente.

https://componentesa17ivansv.neocities.org/ComponenteFormulario/index.html

#### Estructura de Directorios
ComponenteFormulario 
    
    CSS 
        componenteformulario.css
        
    JS
        componenteformulario.js
    
    Tests
        Test1Passed.png
        Test2Passed.png   
    
    index.html
    README.md
    