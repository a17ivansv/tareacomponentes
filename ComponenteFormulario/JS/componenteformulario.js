//Aladimos un evento change al seleccionar la opcion SI en el select de si posee obras de arte se disparara una funcion mostrar
var cambio = document.getElementById('obrasarte');
cambio.addEventListener(
    'change',mostrar,false
);
//Defino un array llamo logines que almacenara los logines almacenados en el localstorage al cargar la pagina
var logines = [];
//La funcion comporbar login comprueba que si el localstorage tiene logines los añade a nuestro array 
function comprobarlogin()
{
    if(localStorage.getItem("login")==null)
    {
    }else
    {
        logines = new Array(0);
        let proba = JSON.parse(localStorage.getItem("login"));
        for(let i in proba)
        {
            logines.push(proba[i]);
        }
    }
}
comprobarlogin();
//La funcion mostrar comprueba si el valor seleccionado es SI, añade la clase para mostrar (ya que por defecto el input aparece oculto)
//si por el contrario el valor seleccionado es NO (seleccion por defecto), elimina la clase de ver la cual muestra el elemento
function mostrar()
{
    if(cambio.value == 'SI')
    {
        document.getElementsByClassName('opcional')[0].classList.add('ver');
        document.getElementsByClassName('opcional')[1].classList.add('ver');
    }else
    {
        document.getElementsByClassName('opcional')[0].classList.remove('ver');
        document.getElementsByClassName('opcional')[1].classList.remove('ver');
    }
}

//Aladimos un evento click al pinchar en el boton enviar, dicho evento disparara una funcion que comprobara si todos los campos necesarios estan cubiertos y si el DNI (primary key)
// no este ya almacenado pues de esta forma ya tendria el login realizado

var enviar = document.getElementById('enviar');
enviar.addEventListener(
    'click',comprobar,false
);


//Defino la funcion comprobar 
function comprobar()
{

    let error = 0;
    let nombre = document.getElementById('nombre');
    let apellido1 = document.getElementById('apellido1');
    let apellido2 = document.getElementById('apellido2');
    let dni = document.getElementById('dni');
    let fnac = document.getElementById('fnac');
    let email = document.getElementById('email');
    let pais = document.getElementById('pais');
    let direccion = document.getElementById('direccion');
    let obrasarte = document.getElementById('obrasarte');

    //Defino un array con los campos necesarios (obigatorios)
    let lista = [nombre,apellido1,apellido2,dni,fnac,email,pais,direccion];
    //Recorro dichos campos, y si el valor es == '', pues error pasa de 0 a 1, luego veremnos que si al finalizar el formulario error ==1 este no se envia y muestra el error
    for(let i in lista)
    {
        if(lista[i].value == '')
        {   
            lista[i].classList.add('error');
            error = 1;

        }else
        {
            lista[i].classList.remove('error');
        }
    }
    //Compruebo que si obras de arte == SI, y el campo numero de obras es == '' entonces hay un error pues dichos campos debe de ir siempre juntos
    let numeroobras = document.getElementById('numero');
    if(obrasarte.value == 'SI' && (numeroobras.value == ''))
    {
        numeroobras.classList.add('error');
        error = 1;
    }else
    {
        numeroobras.classList.remove('error');
    }

    //Si al finalizar la revision error == 0, entonces lo que hago es comprobar que el DNI (PK), no haya sido introducido por otro usuario, pues no puede haber 2 DNI iguales
    if(error == 0)
    {
        let dni = document.getElementById('dni').value;
        let existe = 0;
        for(let i in logines)
        {
            if(dni == logines[i])
            {
                
                existe = 1;
                break;
            }
        }
        //Si el DNI no existe añado al usuario y muestro un mensaje 
        if(existe ==0)
        {
            alert('Gracias por subscribirte a nuestra galeria de arte')
            let dni = document.getElementById('dni').value;
            logines.push(dni);
            let str = JSON.stringify(logines);
            //Añadimos el DNI al local storage
            localStorage.setItem('login',str);
            document.getElementById('enviar').setAttribute('type','submit');

        }else
        {
            //Muestro un mensaje de error informando de que dicho DNI ya existe
            alert("El usuario con DNI : "+dni+ " ya existe");
        }

        
    //Si al finalizar error ==1 pues indicamos al usuario que existen campos obligatorios no cubiertos
    }else
    {
        alert("Existen campos obligatorios no cubiertos");
    }
   
}