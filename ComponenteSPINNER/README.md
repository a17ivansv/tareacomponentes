# ComponenteSpinner
La idea es crear un spinner de carga con una animación para que tenga efecto de giro. Además con JS haremos que dicho spinner desaparezca una vez nuestra pagina web se haya cargado.

Hemos usado la herramienta neocities.org para poder realizar los test de accesibilidad de nuestro componente alcanzando en todos los test exito. (Adjuntamos una captura del resutlado de los test) en el directorio de cada componente. Además tambien se facilita la url de neocities para poder visualizar el componente. Es posible que la visualización desde neocities.org no sea exactamente igual en cuanto a imagenes yy videos con los archivos del .zip debido a que neocities no permite ciertas extensiones de archivos en su version gratuita (.mp4,.jfif,...)

https://componentesa17ivansv.neocities.org/ComponenteSPINNER/index.html


#### Estructura de Directorios
ComponenteSPINNER
    
    CSS 
        componentespinner.css
        
    JS
        componentespinner.js

    Tests
        Test1Passed.png
        Test2Passed.png
        
    index.html
    README.md
    