function docReady(fn) {
    //Comporbamos que el DOM se ha cargado correctamente y esta accesible
    if (document.readyState === "complete" || document.readyState === "interactive") {
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}
var spinnerEL = document.getElementById('spinner');
/* Definimos a función encargada de engadir display:none ao spinner */
var addDisplayNone = (event) => { 
    event.target.style.display="none";
    console.log('Funcion que define el display none del spinner')
    /* Unha vez ocultamos o spinner completamente xa non precisamos
     seguir escoitando si a transición remata, polo que podemos eliminar o listener */
    spinnerEL.removeEventListener('transitionend', addDisplayNone);
    spinnerEL.removeEventListener('webkitTransitionEnd', addDisplayNone);
};

/* Cando o documento está listo pasamos unha función que engade a clase hide
Esta aplica o css opacity:0 que iniciará a transición do spinner para ocultalo */
docReady( () => {
    console.log("Documento Cargado Desaparece Spinner");
    spinnerEL.addEventListener('transitionend',addDisplayNone,false);
    spinnerEL.addEventListener('webkitTransitionEnd',addDisplayNone,false);
    spinnerEL.classList.add('hide');
    });


// A SECUENCIA SERÍA ALGO ASÍ:
// Reload

// Peticións a servidor
// Inicia a descarga de arquivos (img, js, css, html)
// remata a descarga e comenza a renderizar

// Cando a páxina está interactiva e renderizada 
//execútase a función que pasamos como parámetro de docReady

// Spinner inicia a transición para opacity 0

// CANDO REMATA A TRANSICIÓN engadimos display: none;